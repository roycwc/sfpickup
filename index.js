const express = require('express')
const Store = require("jfs");
let db = new Store("data.json");
let app = express()
let regions = db.getSync('regions')
const cors = require('cors')
app.use(cors({origin:true,credentials: true}))

app.get('/regions',(req, res)=>{
	let result = regions.map((region)=>{
		return {
			name: region.name,
			code: region.code
		}
	})
	res.json(result)
})

app.get('/districts/:regionCode',(req, res)=>{
	let requestRegion = regions.find((region)=>{
		return region.code == req.params.regionCode
	})
	let districts = requestRegion.districts.map((district)=>{
		return {
			name: district.name,
			code: district.code,
			parentCode: district.parentCode
		}
	})
	res.json(districts)
})

app.get('/points/:regionCode/:districtCode',(req, res)=>{
	let requestRegion = regions.find((region)=>{
		return region.code == req.params.regionCode
	})
	let requestDistrict = requestRegion.districts.find((district)=>{
		return district.code == req.params.districtCode;
	})
	res.json(requestDistrict.points)
})

app.listen(8210)
