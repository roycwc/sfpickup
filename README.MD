# SF Express Pickup Service Point API
Expose a RESTful service for query SF Express Pickup Service points given by a region / district

> You have to cache all service points using `update.js` before starting the API service

# Requirement
- nodejs v7.4.0+

## RESTful API
- `/regions` - return all regions
- `/districts/:regionId` - return all districts given regionId
- `/points/:regionId/:districtId` - return all service points given regionId, districtId

## Update service point cache
1. Go to Google API console, enable Google Geocode API, get the ApiKey
2. Place the following in `config.json`
```js
// config.json
{
	"googleApiKey":<YOUR_API_KEY>
}
```
3. Run the update script
```
npm install
node --harmony update.js
```

## Run the RESTful API
```
node --harmony .
```
