const Store = require("jfs");
const Config = require("./config.json");
let db = new Store("data.json");
const request = require('request');

async function fetch(url){
	return new Promise((resolve)=>{
		request(url,(a,b,body)=>{
			try{
				resolve(JSON.parse(body))
			}catch(e){
				resolve({})
			}
		})
	})
}

function attachFullName(list, header){
	return list.map((item)=>{
		item.fullname = header + '-' + item.name;
		return item;
	})
}

async function getRegions(){
	const regionUrl = 'http://www.sf-express.com/sf-service-web/service/region/A000810000/subRegions?level=2&lang=tc&region=hk&translate=tc'
	let regions = await fetch(regionUrl);
	return attachFullName(regions, '香港')
}

async function getDistricts(region){
	let districtUrl = 'http://www.sf-express.com/sf-service-web/service/region/'+region.code+'/subRegions?level=4&lang=tc&region=hk&translate=tc';
	let districts = await fetch(districtUrl)
	let districtsWithFullnames = attachFullName(districts, region.fullname)
	return districtsWithFullnames;
}

async function getGeo(fullname){
	let url = 'https://maps.googleapis.com/maps/api/geocode/json?address='+encodeURI(fullname)+'&key='+Config.googleApiKey;
	let result = await fetch(url);
	return result.results[0].geometry.location;
}

async function getPoints(lat, lng, range=2000){
	let url = 'http://www.sf-express.com/sf-service-web/service/store?lang=tc&region=hk&translate=tc&longitude='+lng+'&latitude='+lat+'&range='+range;
	return fetch(url)
}


async function update(){
	let regions = await getRegions()
	regions = await Promise.all(regions.map(async (region)=>{
		region.districts = await getDistricts(region)
		return region;
	}))
	regions = await Promise.all(regions.map(async (region)=>{
		region.districts = await Promise.all(region.districts.map(async (district)=>{
			district.geo = await getGeo(district.fullname)
			return district
		}))
		return region
	}))
	regions = await Promise.all(regions.map(async (region)=>{
		region.districts = await Promise.all(region.districts.map(async(district)=>{
			district.points = await getPoints(district.geo.lat, district.geo.lng)
			return district
		}));
		return region
	}))
	db.saveSync('regions', regions);
}

module.exports = {
	update:update,
	getPoints:getPoints
}
